import { FamilyMember } from './FamilyMember';

export class Runner{

    constructor(
        public id: number,
        public profilePhoto: string|any,
        public firstName: string,
        public middleName: string,
        public lastName: string,
        public birthDate: string,
        public gender: string,
        public email: string,
        public maritalStatus: string,
        public citizenship: string,
        public currentPassword: string,
        public newPassword: string,
        public countryProfileAddress: string,
        public address1ProfileAddress: string,
        public address2ProfileAddress: string,
        public postalCodeProfileAddress: string,
        public cityProfileAddress: string,
        public stateProfileAddress: string,
        public countryMailingAddress: string,
        public address1MailingAddress: string,
        public address2MailingAddress: string,
        public postalCodeMailingAddress: string,
        public cityMailingAddress: string,
        public stateMailingAddress: string,
        public typePrimaryPhone: string,
        public countryPrimaryPhone: string,
        public numberPrimaryPhone: string,
        public typeSecondPhone: string,
        public countrySecondPhone: string,
        public numberSecondPhone: string,
        public nameEmergencyContact: string,
        public typePhoneEmergencyContact: string,
        public countryPhoneEmergencyContact: string,
        public numberPhoneEmergencyContact: string,
        public preferredLanguageEmergencyContact: string,
        public runningAbility: string,
        public clubAffiliation: string,
        public unisexCottonTShirtSize: string,
        public unisexTechShirtSize: string,
        public fittedCottonTShirtSize: string,
        public fittedTechShirtSize: string,
        public ethnicity: string,
        public educationalLevel: string,
        public annualHouseholdIncome: string,
        public industry: string,
        public occupation: string,
        public companyName: string,
        public familyMembers: FamilyMember[]
    ){

    }

}