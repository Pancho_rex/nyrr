export class FamilyMember{

    constructor(
        public id: number,
        public firstName: string,
        public lastName: string,
        public email: string,
        public relationship: string
    ){

    }

}