import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

import { Runner } from '../classes/Runner';
import { FamilyMember } from '../classes/FamilyMember';
import { RunnersApiService } from '../services/RunnersApi.service';

@Component({
  selector: 'app-runner-form-add',
  templateUrl: './runner-form-add.component.html',
  styleUrls: ['./runner-form-add.component.css']
})
export class RunnerFormAddComponent implements OnInit {

  runnerForm: FormGroup;
  runner: Runner = new Runner(0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Array<FamilyMember>());
  familyMember: FamilyMember = new FamilyMember(0,'','','','');
  apiUrl:string = '';
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(
    public fb: FormBuilder,
    private http:Http,
    private router: Router,
    private runnersApiService: RunnersApiService
  ){
    this.runnerForm = this.fb.group({
      profilePhoto: null,
      firstName: ['', [Validators.required]],
      middleName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
      email: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      rePassword: ['', [Validators.required]],
      address1ProfileAddress: ['', [Validators.required]],
      address2ProfileAddress: ['', [Validators.required]],
      postalCodeProfileAddress: ['', [Validators.required]],
      cityProfileAddress: ['', [Validators.required]],
      stateProfileAddress: ['', [Validators.required]],
      address1MailingAddress: ['', [Validators.required]],
      address2MailingAddress: ['', [Validators.required]],
      postalCodeMailingAddress: ['', [Validators.required]],
      cityMailingAddress: ['', [Validators.required]],
      stateMailingAddress: ['', [Validators.required]],
      numberPrimaryPhone: ['', [Validators.required]],
      numberSecondPhone: ['', [Validators.required]],
      nameEmergencyContact: ['', [Validators.required]],
      numberPhoneEmergencyContact: ['', [Validators.required]],
      companyName: ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

  onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.runnerForm.get('profilePhoto').setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        })
      };
    }
  }

  clearFile() {
    this.runnerForm.get('avatar').setValue(null);
    this.fileInput.nativeElement.value = '';
  }

  addFamilyMember(){
    console.log(this.familyMember);
    this.runner.familyMembers.push(this.familyMember);
    this.familyMember = new FamilyMember(0,'','','','');
  }

  submit(){
    console.log("Runner to insert:");
    console.log(this.runner);
    // this.http.get('assets/appConfig.json').subscribe(res => {
    //   this.apiUrl = res.json()[0]['apiUrl'];
    //   this.runnersApiService.addRunner(this.apiUrl, this.runner).subscribe(data => {
    //     console.log("Respuesta: " + data);
    //     alert('El Runner fue registrado con éxito.');
    //     this.router.navigate(['']);
    //   });
    // });
  }
}