import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunnerFormAddComponent } from './runner-form-add.component';

describe('RunnerFormAddComponent', () => {
  let component: RunnerFormAddComponent;
  let fixture: ComponentFixture<RunnerFormAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunnerFormAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunnerFormAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
