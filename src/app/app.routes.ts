import { RouterModule, Routes } from '@angular/router';
import { RunnerFormAddComponent } from './runner-form-add/runner-form-add.component';

const APP_ROUTES: Routes = [
  { path: 'runnerFormAdd', component: RunnerFormAddComponent },
  { path: '', component: RunnerFormAddComponent },
  { path: '*', pathMatch: 'full', redirectTo: 'runnerFormAdd' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});
