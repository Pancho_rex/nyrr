import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import '../rxjs/rxjs';
import { FamilyMember } from '../classes/FamilyMember';
import { Runner } from '../classes/Runner';

@Injectable()
export class RunnersApiService{
    
  constructor(private http: Http){}

  getRunners(apiUrl:string): Observable<Runner[]>{
      return this.http.get(this.getUrl(apiUrl, '')).map(this.getDatos).catch(this.error);
  }

  getRunner(apiUrl:string, id:number): Observable<Runner>{
      return this.http.get(this.getUrl(apiUrl, '/' + id)).map(this.getDato).catch(this.error);
  }

  addRunner(apiUrl:string, model: Runner): Observable<string>{
      return this.http.post(this.getUrl(apiUrl, ''),this.setDato(model)).map(this.getResponse).catch(this.error);
  }

  editRunner(apiUrl:string, model: Runner): Observable<Runner[]>{
      return this.http.put(this.getUrl(apiUrl, '/' + model.id),this.setDato(model)).map(this.getResponse).catch(this.error);
  }

  deleteRunner(apiUrl:string, id: number): Observable<Runner>{
      return this.http.delete(this.getUrl(apiUrl, '/' + id)).map(this.getResponse).catch(this.error);
  }

  private error(error: any){
      console.log(error);
      let msg = (error.message) ? error.message : 'Error desconocido';
      console.error(msg);
      return Observable.throw(msg);
  }

  private getResponse(data: Response){
      let answer = data.json();
      return answer;
  }

  private getDatos(data: Response){
      let datos = data.json()['data'];
      let runners: Array<Runner> = [];
      for(var i = 0;i<datos.length;i++) { 
        let runner: Runner = new Runner(0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Array<FamilyMember>());
        runner.id = datos[i].id;
        runner.firstName = datos[i].firstName;
        runners.push(runner);
      } 
      return runners || [];
  }

  private getDato(data: Response){
      let datos = data.json()['data'];
      let runner: Runner = new Runner(0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Array<FamilyMember>());
      runner.id = datos.id;
      runner.firstName = datos.firstName;
      return runner;
  }

  private setDato(model: Runner){
      let datos: any = 
      {
          "firstName": model.firstName,
      };
      console.log("SetDatos: " + datos);
      return datos;
  }

  private getUrl(apiUrl:string, modelo: String){
      console.log('getUrl: ' + apiUrl);
      return apiUrl + 'runners' + modelo;
  }

}