import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//rutas
import {APP_ROUTING} from './app.routes';

//componentes
import { AppComponent } from './app.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';
import { RunnerFormAddComponent } from './runner-form-add/runner-form-add.component';

// servicios
import { RunnersApiService } from './services/RunnersApi.service';

@NgModule({
  declarations: [
    AppComponent,
    AppNavbarComponent,
    RunnerFormAddComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    APP_ROUTING
  ],
  providers: [
    RunnersApiService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
